resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPvZez7nS1QBLc1GcxtgHIGF6MdAFydCyCuX9EDG6XbXNfspofF4Ua587XDINMjdVmF2Jf9nM66Ik6rBrqdWMxsVqimM8IQWq/tHucwCyB9y8P5hTdM9hJKitHDroWfbMz/R73va9zQp2bcPLTTwW9vRlI6lRthjAygz2Yi6SlDneS+dL3MtMYuFvzn4ftfIC5vmbOW7BcCp4GMvYGi+ytlRN+qgc+9O6a3m1RhYpfFn8DqYZDKXno5NEfTZRtIvE+/Jrd2A/tb4TDBEJ94H/tkm9jjANhmTPq4o+7KeqFAFpOAVmwK4QiTCrxpEo4L1xd/C+P0YK+kPI2e1A3MG50xBtHsxHr1/NtCrqxwUILwSG0aXK5tDw1vxQiwzWBsKP2vnfLkfPRBFmh28ccHadzFvTkUXr3sH0Sl/2218c+D3OwJlUPT1M0om3CPybNwM3B0wywxqnMxz+LV7D6mpMHZ1f/0q11exMb0w2yQnV5UEwjY0gQVCsNUf3DBz55Gq8= lucas@ubuntu"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}


resource "aws_vpc" "mainvpc" {
  cidr_block = "172.31.0.0/16"
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"

  tags = {
    Name = "DevOpsTp1"
  }
}